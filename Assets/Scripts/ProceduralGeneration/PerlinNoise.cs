﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerlinNoise
{
    int _terrainHeight;
    int _terrainWidth;
    float[] _fNoiseSeed2D;

    int _nOctaves = 1;
    float _fBias = 2.0f;
    int nMode = 1; // not sure =)

    [SerializeField]
    public float[] _fResult;

    public void SetUpPerlinNoise(int i_height, int i_width)
    {
        this._terrainHeight = i_height;
        this._terrainWidth = i_width;

        _fNoiseSeed2D = new float[_terrainWidth * _terrainHeight];
        _fResult = new float[_terrainWidth * _terrainHeight * 6];

        for (int i = 0; i < _terrainHeight * _terrainWidth; i++)
        {                                   // generate nr. between 0 and 1
            _fNoiseSeed2D[i] = Random.Range(0.0f, float.MaxValue) / float.MaxValue;
            // Debug.Log("RandValue: " + _fNoiseSeed2D[i]);
        }
    }

    public void perlinNoise2D()
    {
        for(int x = 0; x < _terrainWidth; x++)
        {
            for (int y = 0; y < _terrainHeight; y++)
            {
                float fNoise = 0.0f;
                float fScaleAcc = 0.0f;
                float fScale = 1.0f;

                for (int o = 0; o < _nOctaves; o++)
                {
                    // for each octave we need two sample points that we can 
                    // linearly interpolate between. The distance between this 
                    // sample points its called the ptich

                    // if we make sure that the perlin noise is always inn the
                    // power of 2, we can always half our pitch easly.

                    int nPitch = _terrainWidth >> o;
                    int nSampleX1 = (x / nPitch) * nPitch;
                    int nSampleY1 = (y / nPitch) * nPitch;

                    int nSampleX2 = (nSampleX1 + nPitch) % _terrainWidth;
                    int nSampleY2 = (nSampleY1 + nPitch) % _terrainWidth;

                    float fBlendX = (float)(x - nSampleX1) / (float)nPitch;
                    float fBlendY = (float)(y - nSampleY1) / (float)nPitch;

                    float fSampleT = (1.0f - fBlendX) * _fNoiseSeed2D[nSampleY1 * _terrainWidth + nSampleX1] + fBlendX * _fNoiseSeed2D[nSampleY1 * _terrainWidth + nSampleX2];
                    float fSampleB = (1.0f - fBlendX) * _fNoiseSeed2D[nSampleY2 * _terrainWidth + nSampleX1] + fBlendX * _fNoiseSeed2D[nSampleY2 * _terrainWidth + nSampleX2];

                    fScaleAcc += fScale;
                    fNoise += (fBlendY * (fSampleB - fSampleT) + fSampleT) * fScale;
                    fScale = fScale / _fBias;
                }
                // Scale to seed range
                _fResult[y * _terrainWidth + x] = fNoise / fScaleAcc;
                // Debug.Log("Result Value: " + _fResult[y * _terrainWidth + x]);
            }
        }
    }
}
