﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fishable : MonoBehaviour
{
    public float spawnTime = 1f;
    private float currentTime = 0f;
    public enum FishTypes { Salmon, Squid };
    public bool available = true;
    Player player;
    float fishRates;

    private void OnTriggerEnter(Collider other)
    {
        
        
        
        if (other.CompareTag("Detection"))
        {
            available = false;
            player = other.gameObject.transform.root.gameObject.GetComponent<Player>();
            if(player)
            {
                player.withinFishingRange = true;
                fishRates = player.fishRates;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        
        if (other.CompareTag("Detection") && player.isFishing)
        {
            currentTime += Time.deltaTime;
            if (currentTime >= spawnTime)
            {
                //player receives ore
                currentTime = 0f;
                if (Random.value < fishRates)
                    player.GetItem("fish");
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Detection"))
        {
            available = true;
            Player player = other.gameObject.transform.root.gameObject.GetComponent<Player>();
            if (player)
            {
                player.withinFishingRange = false;
            }
            
        }
    }
}
