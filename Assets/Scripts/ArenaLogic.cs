﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;

public class ArenaLogic : MonoBehaviour
{
    
    private Manager manager;
    public string enemyPrefab = "Skelekton";
    public int levels;
    private bool available = true;
    private int currentLvl = 1;
    List<GameObject> enemies = new List<GameObject>();
    int enemyCount = 0;
    GameObject player;
    bool won = false;
    Transform entrance;
    public AudioClip enterSound;
    public AudioClip winSound;

    void Start()
    {
        manager = GameObject.Find("Manager").GetComponent<Manager>();
        entrance = transform.root.gameObject.transform.Find("Entrance").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if(!available && !won)
        {
            CheckFinished();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Player"))
        {
            if (available && !won)
            {
                GameObject.Find("AudioManager").GetComponent<AudioManager>().ChangeBGM(enterSound);
                player = other.gameObject;
                StartRound();
            }
        }
    }
    private void CheckFinished()
    {
        int total = 0;
        foreach (GameObject cEnemy in enemies)
        {
            if (!cEnemy) total++;
        }
        
        if (total >= enemies.Count)
        {
            ArenaWon();
        }
       // else Debug.Log(enemies.Count);
    }

    private void ArenaWon()
    {
        GameObject.Find("AudioManager").GetComponent<AudioManager>().ChangeBGM(winSound);
        won = true;
        Debug.Log("YOU WON THIS LEVEL");
        GameObject.Find("UI/Canvas/ArenaText").GetComponent<TextMeshProUGUI>().text = "You beat stage "+currentLvl.ToString()+"!";
        StartCoroutine(ExecuteAfterTime(3));

        //player.transform.position = entrance.position;

    }
    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        available = true;
        player.GetComponent<Player>().inArena = false;

        
        won = false;
        GameObject.Find("UI/Canvas/ArenaText").GetComponent<TextMeshProUGUI>().text = "";
        player.GetComponent<Player>().GetGold(100 * currentLvl);
        currentLvl++;
        player.transform.position = GameObject.Find("insidehouse").transform.position;
        GameObject.Find("AudioManager").GetComponent<AudioManager>().ResetBGM();
        // Code to execute after the delay
    }

    IEnumerator ExecuteAfterTime2(float time)
    {
        yield return new WaitForSeconds(time);
        available = true;
        player.GetComponent<Player>().inArena = false;
        player.GetComponent<Player>().isDead = false;
        player.gameObject.transform.root.gameObject.transform.Find("WK_HeavyIntantry").GetComponent<SkinnedMeshRenderer>().enabled = true;
        player.transform.position = entrance.position;
        won = false;
        foreach (GameObject cEnemy in enemies)
        {
            if (cEnemy) PhotonNetwork.Destroy(cEnemy);
        }
        currentLvl = 1;
        GameObject.Find("UI/Canvas/ArenaText").GetComponent<TextMeshProUGUI>().text = "";
        GameObject.Find("AudioManager").GetComponent<AudioManager>().ResetBGM();
        // Code to execute after the delay
    }

    public void Died()
    {
        GameObject.Find("UI/Canvas/ArenaText").GetComponent<TextMeshProUGUI>().text = "You lost!";
        player.gameObject.transform.root.gameObject.transform.Find("WK_HeavyIntantry").GetComponent<SkinnedMeshRenderer>().enabled = false;
        player.GetComponent<Player>().isDead = true;
        StartCoroutine(ExecuteAfterTime2(3));

    }
    public void StartRound()
    {
        player.GetComponent<Player>().inArena = true;
        available = false;
        enemies = new List<GameObject>();
        string cLvl = "level" + currentLvl.ToString();
        Debug.Log(cLvl);

        for (int i = 0; i < transform.Find(cLvl).childCount; i++)
        {
            enemyCount++;
            if (transform.Find(cLvl).GetChild(i).name == "enemyspawn")
            {
                //spawn skele
                GameObject enemy = PhotonNetwork.Instantiate(enemyPrefab, transform.Find(cLvl).GetChild(i).transform.position, transform.Find(cLvl).GetChild(i).transform.rotation);
                enemies.Add(enemy);
               // Debug.Log(transform.Find(cLvl).GetChild(i).transform.position);

            }
            else if (transform.Find(cLvl).GetChild(i).name == "enemyspawn2")
            {
                //spawn mini boss
                GameObject enemy = PhotonNetwork.Instantiate(enemyPrefab, transform.Find(cLvl).GetChild(i).transform.position, transform.Find(cLvl).GetChild(i).transform.rotation);
                enemy.transform.localScale *= 2;
                enemies.Add(enemy);
            }
            else if (transform.Find(cLvl).GetChild(i).name == "bosspawn")
            {
                //spawn mini boss
                GameObject enemy = PhotonNetwork.Instantiate(enemyPrefab, transform.Find(cLvl).GetChild(i).transform.position, transform.Find(cLvl).GetChild(i).transform.rotation);
                enemy.transform.localScale *= 5;
                enemies.Add(enemy);
            }
            //else Debug.Log(transform.Find(cLvl).GetChild(i).name);
        }

        
    }
}
