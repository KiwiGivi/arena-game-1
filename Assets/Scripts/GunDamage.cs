﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GunDamage : MonoBehaviourPunCallbacks
{
    public int damageAmount = 5;
    float targetDistance;
    float fireRate = 0.25f;
    float range = 50f;
    public float hitForce = 100f;
    public Transform gunEnd;

    private Camera fpsCam;
    private WaitForSeconds shotDuration = new WaitForSeconds(0.05f);
    private LineRenderer laserLine;
    private float nextFire;

    AudioSource gunSound;
    Animation gunAnimation;

    void Start()
    {
        laserLine = GetComponent<LineRenderer>();
        fpsCam = GetComponentInParent<Camera>();
        gunSound = GetComponent<AudioSource>();
        gunAnimation = GetComponent<Animation>();
    }

    void Update()
    {
        if (!photonView.IsMine) return;
        if (Input.GetButtonDown("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            StartCoroutine(ShotEffect());

            Vector3 rayOrigin = fpsCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0.0f));
            RaycastHit hit;

            laserLine.SetPosition(0, gunEnd.position);

            if (Physics.Raycast(rayOrigin, fpsCam.transform.forward, out hit, range))
            { 
                laserLine.SetPosition(1, hit.point);

                Shootable health = hit.collider.GetComponent<Shootable>();

                if(health != null)
                {
                    health.Damage(damageAmount);
                }

                if(hit.rigidbody != null)
                {
                    hit.rigidbody.AddForce(Vector3.up * 30.0f);
                    hit.rigidbody.AddForce(-hit.normal * 10.0f);
                }
            }
            else
            {
                laserLine.SetPosition(1, rayOrigin + (fpsCam.transform.forward * range));
            }
        }
    }

    private IEnumerator ShotEffect()
    {
        
        gunSound.Play();
        gunAnimation.Play();
        
        laserLine.enabled = true;
        yield return shotDuration;
        laserLine.enabled = false;
    }
}
