﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    public float cameraMoveSpeed = 120f;
    public GameObject cameraFollowObject;
    Vector3 followPos;
    public float clampAngle = 65f;
    public float inputSensitivity = 150f;
    public GameObject cameraObject;
    public GameObject playerObject;
    public float camDistanceXToPlayer;
    public float camDistanceYToPlayer;
    public float camDistanceZToPlayer;
    public float mouseX;
    public float mouseY;
    public float finalInputX;
    public float finalInputZ;
    public float smoothX;
    public float smoothY;
    private float rotationX = 0f;
    private float rotationY = 0f;
    private float playerTurn;
    Player player;

    

    private void Start()
    {
        Vector3 rot = transform.rotation.eulerAngles;
        rotationX = rot.x;
        rotationY = rot.y;
        playerTurn = rotationX;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        player = transform.root.gameObject.GetComponent<Player>();
    }

    private void Update()
    {
        if (!player.canMove) return;

        mouseX = Input.GetAxis("Mouse X");
        mouseY = Input.GetAxis("Mouse Y");

        finalInputZ = mouseX;
        finalInputX = mouseY;

        rotationX += finalInputX * inputSensitivity * Time.deltaTime;
        playerTurn += finalInputZ * inputSensitivity * Time.deltaTime;

        rotationX = Mathf.Clamp(rotationX, -10f, clampAngle);
        //playerTurn = Mathf.Clamp(rotationY, -clampAngle, clampAngle);

        Quaternion localRot = Quaternion.Euler(rotationX, playerTurn, 0f);
        playerObject.transform.rotation = Quaternion.Euler(playerObject.transform.localRotation.x, playerTurn, playerObject.transform.localRotation.z);
        //Debug.Log(playerTurn);
        transform.rotation = localRot;


    }

    private void LateUpdate()
    {
        CameraUpdater();
    }

    void CameraUpdater()
    {
        Transform target = cameraFollowObject.transform;
        float step = cameraMoveSpeed * Time.deltaTime;
        
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);
    }


}
