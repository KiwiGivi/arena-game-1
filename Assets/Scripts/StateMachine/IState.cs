﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public interface IState
{
    void StateEnter();
    void StateUpdate();

    /*
     * OnFixedUpdate(), for physics related stuff.
     * 
     */

   void StateExit();
}
