﻿using UnityEngine.Audio;
using UnityEngine;
using System;
public class AudioManager : MonoBehaviour
{

    public AudioSource BackGroundMusic;
    public AudioClip mainTheme;

    public void Start()
    {
        mainTheme = BackGroundMusic.clip;
    }

    public void Update()
    {
        
    }

    public void ChangeBGM(AudioClip music)
    {
        BackGroundMusic.Stop();
        BackGroundMusic.clip = music;
        BackGroundMusic.Play();
    }

    public void ResetBGM()
    {
        BackGroundMusic.Stop();
        BackGroundMusic.clip = mainTheme;
        BackGroundMusic.Play();
    }
    

}
