﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    #region Singleton

    public static PlayerManager m_instance;

    private void Awake()
    {
        m_instance = this;
    }

    #endregion

    // should also be an list or array
    public GameObject m_player;

    // Sets the player target, should be called when the player is spawned on the server
    // also might have to make it so the scripts keeps track of multiple players.
    public void AddPlayerTarget(GameObject i_player)
    {
        m_player = i_player;
    }
   
}


