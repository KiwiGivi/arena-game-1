﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    //bool reachedGoal = false;

    float _speed = 100.0f;
    float _turnSpeed = 1.0f;

    [SerializeField]
    WayPointSystem _wayPointSystem;

    [SerializeField]
    WayPoint _wayPointWalkingTo;

    public List<WayPoint> _WayPoints;

    // ASTAR
    [SerializeField]
    public WayPoint _startNode;

    [SerializeField]
    public WayPoint _goalNode;

    [SerializeField]
    public List<WayPoint> _openList;
    [SerializeField]
    public List<WayPoint> _closedList;

    [SerializeField]
    public List<int> path = new List<int>();
    public int _pathLength;
    bool _done = false;

    public void Awake()
    {
        GameObject _wpSystem = GameObject.Find("WayPointSystem");
        WayPointSystem _wpScript = _wpSystem.GetComponent<WayPointSystem>();

        _WayPoints = _wpScript.wayPoints;
    }

    // Start is called before the first frame update
    void Start()
    {
        AStar();
        _done = false;
        _pathLength = path.Count;

        _wayPointWalkingTo = _startNode;
    }

    // Update is called once per frame
    void Update()
    {
        if(!_done)
        {
            if (Vector3.Distance(this.transform.position, _wayPointWalkingTo.transform.position) < 0.1f)
            {
                _wayPointWalkingTo = _WayPoints[path[--_pathLength]];
                if(_pathLength == 0)
                {
                    _done = true;
                }
            }
        }
        Move();
    }

    public void Move()
    {
        // 1. Turn Towards destination
        // Calculate direction 
        // Vector3 movDifference = _wayPoint.transform.position - this.transform.position;
        // Vector3 movDirection = movDifference.normalized;
        Vector3 direction = _wayPointWalkingTo.transform.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(direction);
        this.transform.rotation = Quaternion.Lerp(transform.rotation, rotation, _turnSpeed * Time.deltaTime);

        // 2. Move towards destination
        float _step = _speed * Time.deltaTime;
        this.transform.position = Vector3.MoveTowards(transform.position, _wayPointWalkingTo.transform.position, Time.deltaTime * _step);
    }

    // Move to GameObject's positon
    // Move to  Node/waypoint
    public void SetTargetWaypoint()
    {

    }

    public void AStar()
    {
        // Sett the indexs for all nodes
        for (int i = 0; i < _WayPoints.Count; i++)
        {
            _WayPoints[i]._index = i;
        }

        Debug.Log("Starting ASTAR");

        // 1. Find WayPoint closest to Bot
        WayPoint _closestNode = _WayPoints[0];
        float f_closest = Vector3.Distance(_WayPoints[0].transform.position, this.transform.position);
        int _closestIndex = 0;

        for (int i = 0; i < _WayPoints.Count; i++)
        {
            if (Vector3.Distance(_WayPoints[i].transform.position, this.transform.position) < f_closest)
            {
                f_closest = Vector3.Distance(_WayPoints[i].transform.position, this.transform.position);
                _closestNode = _WayPoints[i];
                _closestIndex = i;
            }
        }
        // start with the waypoint closes to the character this is: 
        _startNode = _closestNode;

        // 2.  Set wayPoint

        // 3. Perform A*

        _openList.Add(_startNode);
        _startNode._gCost = 0;  // Starting node has gCost of 0
        _startNode._hCost = Vector3.Distance(_startNode.transform.position, _goalNode.transform.position);
        WayPoint _currentNode = _startNode; // Node with lowest fScore

        while (_openList.Count != 0)
        {
            if (_currentNode == _goalNode)
            {
                reconstruct_path(_currentNode);
            }

            _openList.Remove(_currentNode);
            _closedList.Add(_currentNode);

            foreach (var node in _currentNode.connections)
            {
                // Node is already checked
                if (_closedList.Contains(node))
                {
                    continue;
                }

                if (_openList.Contains(node))
                {
                    // check if new path is better
                    float newGCost = node._gCost + _currentNode._gCost;
                    if (newGCost < node._gCost)
                    {
                        // Path is better, update parent and cost.
                        node._gCost = newGCost;
                        node._parent = _currentNode;
                        node._fCost = node._gCost + node._hCost;
                        node._parent = _currentNode;
                    }
                }
                else
                {
                    _openList.Add(node);
                    node._gCost = _currentNode._gCost + Vector3.Distance(node.transform.position, _currentNode.transform.position);
                    node._hCost = Vector3.Distance(node.transform.position, _goalNode.transform.position); ;
                    node._fCost = node._gCost + node._hCost;
                    node._parent = _currentNode;
                }
            }

            if (_openList.Count != 0)
            {
                // Find the next node, i.e the one with the lowest f.cost
                _currentNode = _openList[0];
                for (int i = 0; i < _openList.Count; i++)
                {
                    if (_currentNode._fCost > _openList[i]._fCost)
                    {
                        _currentNode = _openList[i];
                    }
                }
            }
        }

        // This needs to be called before calling A* again
        // path.Clear();
        //_openList.Clear();
        //_closedList.Clear();

    } 

    public void reconstruct_path(WayPoint current)
    {
        Debug.Log("FOUND A PATH!");
        Debug.Log("PATH IS: ");

        int i = 1;

        // path.Add(_goalNode._index); dont think this is needed
        //                             since we know the target/goal node already.
        path.Add(current._index);
        while (current._parent != _startNode)
        {

            Debug.Log("PATH NODE NUMBER " + i + "is" + current._parent.name);
            current = current._parent;
            path.Add(current._index);
            i++;
        }
        Debug.Log("Start node was: " + current._parent.name);
        path.Add(_startNode._index);

    }
}
