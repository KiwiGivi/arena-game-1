﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class EnemySpawn : MonoBehaviour
{
    // Start is called before the first frame update
    public string enemyPrefab = "Skelekton";
    GameObject enemy;
    float respawnTime = 60f;
    float currentTime = 0f;
    void Start()
    {
        enemy = PhotonNetwork.Instantiate(enemyPrefab, transform.position, transform.rotation);
    }

    // Update is called once per frame
    void Update()
    {
        if(!enemy)
        {
            currentTime += Time.deltaTime;
            if(currentTime >= respawnTime)
            {
                currentTime = 0f;
                enemy = PhotonNetwork.Instantiate(enemyPrefab, transform.position, transform.rotation);
            }
        }
    }
}
