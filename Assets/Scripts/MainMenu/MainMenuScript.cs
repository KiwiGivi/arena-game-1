﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    public void NewGame()
    {
        Debug.Log("Starting new game.", this);
        SceneManager.LoadScene("Menu");
    }

    // Does not work when called inside of the unity editor
    public void ExitGame()
    {
        Debug.Log("Exiting Game..");
        Application.Quit();
    }
}
